# modsman

A Minecraft mod manager for the command line.

## Installation

This utility requires Python 3.7.

```bash
pip install modsman
```

## Usage

```bash
modsman --help
```

## Demos

Linux/macOS bash:

[![asciicast](https://asciinema.org/a/hkKHmQRwcfbMathQMmzxUhKVQ.svg)](https://asciinema.org/a/hkKHmQRwcfbMathQMmzxUhKVQ)

Windows powershell:

[![modsman on windows](https://img.youtube.com/vi/E1Fd5H-JTuQ/0.jpg)](https://www.youtube.com/watch?v=E1Fd5H-JTuQ)